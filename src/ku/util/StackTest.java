package ku.util;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
/**
 * StackTest for testing StackFactory.
 * @author Kasidit Phoncharoen
 *
 */
public class StackTest {
	private Stack stack;
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {
		StackFactory.setStackType( 1 );
		stack = StackFactory.makeStack( 3 );
	}
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}
	@Test
	public void testPeak(){
		stack.push("one");
		assertEquals("one",stack.peek());
		stack.push("two");
		assertEquals("two",stack.peek());
		stack.push("three");
		assertEquals("three",stack.peek());
	}
	/** peek the same element 2 times and should be the same value.*/
	@Test
	public void testDoublePeek(){
		stack.push("one");
		String test = (String) stack.peek();
		assertEquals(test, stack.peek());
		assertEquals(test, stack.peek());
	}
	/** peek empty stack and should return null.*/
	@Test
	public void testEmptyPeak(){
		assertNull(stack.peek());
	}
	/** push one object and isEmpty should return false. */
	@Test
	public void testIsEmpty(){
		assertTrue(stack.isEmpty());
		stack.push("one");
		assertFalse(stack.isEmpty());
	}
	/** push until full and test isFull should return false. */
	@Test
	public void testIsFull(){
		assertFalse(stack.isFull());
		stack.push("one");
		stack.push("two");
		stack.push("three");
		assertTrue(stack.isFull());
	}
	/** When push the size should return the same as the number that push. */
	@Test
	public void testSize(){
		stack.push("one");
		assertSame(1, stack.size());
		stack.push("two");
		assertSame(2, stack.size());
		stack.push("three");
		assertSame(3, stack.size());
	}
	/** The size should return 0 when there is no item.*/
	@Test
	public void testEmptySize(){
		assertEquals(0, stack.size());
	}
	/** The capacity should return the same as capacity.*/
	@Test
	public void testCapacity(){
		assertSame(3, stack.capacity());
	}
	/** The stack should throw IllegalArgumentException when capacity is negative. */
	@Test (timeout=100,expected=java.lang.IllegalArgumentException.class)
	public void testNegativeCapacity(){
		stack = StackFactory.makeStack( -2 );
	}
	/** can't push null and should return IllegalArgumentException.*/
	@Test(timeout=100,expected=java.lang.IllegalArgumentException.class)
	public void testNullPush(){
		stack.push(null);
	}
	/** can't push over size and should throw IllegalStateException.*/
	@Test(timeout=100,expected = java.lang.IllegalStateException.class)
	public void testPushOverSize(){
		stack.push("one");
		stack.push("one");
		stack.push("one");
		stack.push("one");
	}
	/** the value that pop that pop should return the same calue that push.*/
	@Test
	public void testNormalPop(){
		stack.push("one");
		stack.push("two");
		stack.push("three");
		assertSame("three",stack.pop());
		assertSame("two", stack.pop());
		assertSame("one",stack.pop());
	}
	/** should throw EmptyStackException when pop empty stack.*/
	@Test(timeout=100, expected =  java.util.EmptyStackException.class)
	public void testEmptyPop(){
		stack.pop();
	}
}